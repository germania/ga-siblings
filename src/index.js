//
// Stolen from PlainJS.com's siblings solution:
// https://plainjs.com/javascript/traversing/get-siblings-of-an-element-40/
//
module.exports = function(el, filter) {
    var siblings = [];
    el = el.parentNode.firstChild;
    do {
        if (!filter || filter(el)) {
            siblings.push(el);
        }
    } while (el = el.nextSibling);
    return siblings;
};
