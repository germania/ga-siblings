#ga-siblings

Get *all* siblings of an element.

This solution as well as usage notes below brazenly stolen from [PlainJS.com's siblings solution](https://plainjs.com/javascript/traversing/get-siblings-of-an-element-40/). See full documentation there.


##Installation

```bash
npm install ga-siblings --save
```

##Usage

```js
// Require module
var getSiblings = require('ga-siblings');

// Get all siblings of el:
el = document.querySelector('div');
var sibs = getSiblings(el);
```

###Optional filtering

```js
// Example filter function
function exampleFilter(el) {
    return el.nodeName.toLowerCase() == 'a';
}

// Get only anchor element siblings of el
el = document.querySelector('div');
var sibs_a = getSiblings(el, exampleFilter);

```


##Develop with Gulp

Use [Git Flow](https://github.com/nvie/gitflow), always work in `develop` branch.

- Install development dependencies: `npm install`
- Run `gulp watch`
- Work in `src/`

Builds are now in `dist/`